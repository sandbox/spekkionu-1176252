(function ($) {
  // Read cookie
  var site_counter_cookie = $.cookie('site_counter_token');
  if(!site_counter_cookie){
    // Increment counter if cookie does not exist
    Drupal.behaviors.site_counter = {
      attach: function (context, settings) {
        $.post(Drupal.settings.basePath+'site_counter/record', function(response){
          if(response.success){
            $('#visitor-counter').html(response.total);
          }
        }, 'json'
      );
      }
    };
  }else{
    // The cookie exists, only pull value
    Drupal.behaviors.site_counter = {
      attach: function (context, settings) {
        $.getJSON(Drupal.settings.basePath+'site_counter/total',  function(response){
            if(response.success){
              $('#visitor-counter').html(response.total);
            }
        });
      }
    };
  }
})(jQuery);